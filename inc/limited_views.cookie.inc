<?php

/**
 * @file
 *  Contains Cookie tracking method
 */

/**
 * Increments a user's view count by arbitrary figure
 *
 * @param int $nid
 *  Node ID of current node
 * @param int $add
 *  figure to add to the existing count
 * @return int $count
 *  Number of views after new addition
 */
function limited_views_add($nid, $add = 1) {
  static $progressive;
  if (is_null($progressive)) {
    $progressive = limited_views_get_view_count();
  }
  $progressive += $add;

  $cookie_timestamp = 'limited_views_count['. $nid .'][timestamp]';
  setcookie($cookie_timestamp, time(), time() + LIMITED_VIEWS_EXPIRY_TIME);
  $cookie_count = 'limited_views_count['. $nid .'][count]';
  setcookie($cookie_count, $add, time() + LIMITED_VIEWS_EXPIRY_TIME);
  return $progressive;
}

/**
 * Returns the number of views a user has accrued on their cookie
 *
 * This should always be used to access the view count, as it sanitizes the user
 * provided data.
 *
 * To use this in a test, you will have to use === in your statement to force
 * type checking.
 *
 * @return int
 *  number of content views
 */
function limited_views_get_view_count() {
  $expiry_timestamp = time() - LIMITED_VIEWS_EXPIRY_TIME;
  $view_count = 0;
  $count_data = (is_array($_COOKIE['limited_views_count'])) ? $_COOKIE['limited_views_count'] : array();
  foreach ($count_data as $nid => $data) {
    if ($data['timestamp'] > $expire_timestamp) {
      // only add the count if the expiry time is less than the expiry time
      $view_count += (int)$data['count'];
    }
  }

  return $view_count;
}

/**
 * Tests if the user has cookies enabled and has a limited_views cookie
 *
 * Beware, this causes a full page refresh if the user doesn't have the cookie yet
 */
function limited_views_check_method() {
  if (is_array($_COOKIE['limited_views_count'])) {
    // user already has a count cookie
    return TRUE;
  }
  else {
    $cookies_enabled = (bool)$_COOKIE['limited_views_cookie_enabled'];
    if (empty($cookies_enabled) && !$_GET['cookie_check']) {
      // set a test cookie if the cookie doesnt exist, and the test isn't already running
      setcookie('limited_views_cookie_enabled', TRUE);
      $query = array('cookie_check' => 1);

      // Make sure we include other query components in the redirect
      $url = explode('?', trim($_SERVER['REQUEST_URI'], '/'));
      $other_queries = explode('&', $url[1]);
      foreach ($other_queries as $string) {
        $parts = explode('=', $string);
        $query[$parts[0]] = $parts[1];
      }
      drupal_goto($url[0], $query);
    }
    if (empty($cookies_enabled) && $_GET['cookie_check']) {
      // the test is running and the cookie doesn't exist, user is not
      // accepting our cookie
      return FALSE;
    }
    if ($cookies_enabled) {
      // client is accepting our cookie
      return TRUE;
    }
  }
}

/**
 * Displays a message telling the user a problem exists with the tracking method
 */
function limited_views_tracking_method_unavailable() {
  return 'You must enable cookies to view this content.';
}
